import React, { useState, useEffect } from 'react';
import { Switch, Route, Router } from 'react-router-dom'
import Floor from './component/Page/Floor'
import FloorDetail from './component/Page/FloorDetail'
import Shop from './component/Page/Shop'
import ShopDetail from './component/Page/ShopDetail'
import HomePage from './component/Page/Home'
import NOtFound from './NotFound'
import './App.css';

const App = props => {
  return (
    <div className="App">
      <Switch>
        <Route path="/shop/:id" component={ShopDetail} />
        <Route path="/shop/" component={Shop} />
        <Route path="/floor/:id" component={FloorDetail} />
        <Route path="/floor" component={Floor} />
        <Route path="/" component={HomePage} />
        <Route component={NOtFound} />
      </Switch>
    </div>
  );
}
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
