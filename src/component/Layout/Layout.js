import React, { useState, useEffect } from 'react'
import Topmenu from './Topmenu'
import classNames from 'classnames'
import { Container, Card, Table, Col } from 'reactstrap'

const Layout = props => {
    const [isOpen, setOpen] = useState(false)
    const toggle = () => {
        window.localStorage.setItem('side_bar', !isOpen)
        setOpen(!isOpen)
    }

    return (
        <>
            <Topmenu />
            {props.children}
        </>
    )
}

export default Layout
