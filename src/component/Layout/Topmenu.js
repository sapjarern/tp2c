import React, { useState } from 'react';
import Logo from '../Image/logo.png'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarText } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Topmenu.css'

const Topmenu = props => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand href="/"><img src={Logo} className='nav'/></NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink href="/floor">See Floor</NavLink>
                    </NavItem>

                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Floor
                        </DropdownToggle>
                        <DropdownMenu >
                            <DropdownItem>
                                <Link to='/floor/1'>Floor 1</Link>
                            </DropdownItem>
                            <DropdownItem>
                                <Link to='/floor/2'>Floor 2</Link>
                            </DropdownItem>
                            <DropdownItem>
                                <Link to='/floor/3'>Floor 3</Link>
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </Collapse>
        </Navbar>
    );
}

export default Topmenu;
