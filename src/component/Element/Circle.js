import React, { useState, useEffect } from 'react';

const Circle = props => {
    let circlestyle = {
        padding: 10,
        margin: 20,
        display: "inline-block",
        backgroundColor: props.bgColor,
        borderRadius: "50%",
        width: 100,
        height: 100,
    }
    return(
        <div style={circlestyle}></div>
    );
}

export default Circle