import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { useParams, Link } from "react-router-dom";
import Pen1 from '../Image/item/pen.jpg'
import Pen2 from '../Image/item/pen2.jpg'
import Note from '../Image/item/note.jpg'
import Ruler from '../Image/item/ruler.jpg'
import Layout from '../Layout/Layout';
import ReactFlowPlayer from "react-flow-player";
import './ShopDetail.css'


const ShopDetail = props => {
    const { id } = useParams();
    return (
        <Layout>
            <h3>คุณกำลังอยู่ที่ร้าน {id}</h3>
            <br />
            <br />
            <hr />

            <div className="row">

                <div className="col-md-1"> </div>
                <div className="col-md-8">

                    <ReactFlowPlayer
                        sources={[

                            {
                                type: 'video/flash',
                                src: "rtmp://34.87.157.188/live/store_cam",
                            }]}
                        live={true}
                        rtmp="rtmp://34.87.157.188/live/store_cam"
                        playerId="reactFlowPlayer"
                        playerInitScript="http://releases.flowplayer.org/7.2.1/flowplayer.min.js"
                        autoplay={true}
                        title={id}
                        licenseKey=""
                    />

                </div>


                <div className="col-md-3">
                    <div className="row"> <h2> ร้าน: {id} </h2>  </div>
                    <div className="row"> สินค้าน่าสนใจ</div>
                    <div className="row">
                        <img src={Pen1} className='item' />
                        <img src={Pen2} className='item' />
                        <img src={Note} className='item' />
                        <img src={Ruler} className='item' />
                    </div>
                    <div className="row"> <h3> หมวดหมู่: ร้านขายของเบ็ดเตล็ด </h3> </div>
                    <div className="row">  <a href=""> สั่งซื้อของผ่านระบบ E-Store </a> </div>
                    <div className="row">  <a href=""> ติดต่อพนักงานร้านเพื่อซื้อของ </a></div>
                    <ReactFlowPlayer
                        sources={[

                            {
                                type: 'video/flash',
                                src: "rtmp://34.87.157.188/live/seller_cam",
                            }]}
                        live={true}
                        rtmp="rtmp://34.87.157.188/live/seller_cam"
                        playerId="reactFlowPlayerseller_cam"
                        playerInitScript="http://releases.flowplayer.org/7.2.1/flowplayer.min.js"
                        autoplay={true}
                        title={id}
                        licenseKey=""
                    />

                </div>


            </div>
        </Layout>
    );
}

export default ShopDetail;
