import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Badge } from 'reactstrap';
import { useParams, Link, useHistory } from "react-router-dom";
import ImageMapper from 'react-image-mapper';
import SvgFloor from '../Image/SvgFloor'
import Layout from '../Layout/Layout'
import Circle from '../Element/Circle'
//import IFloor1 from '../Image/floor1.jpg'
import IFloor1 from '../Image/floor.png'
import IFloor2 from '../Image/floor2.jpg'
import IFloor3 from '../Image/floor3.jpg'
import floor from '../Image/floor.svg';
import './FloorDetail.css';

const Floor = props => {
    const [Value, setValue] = useState(0)
    const percent = ['#00ff5599', '#aaff5599', '#ffff5599', '#ff7d2299', '#ff002299']
    const hValue = [{ name: "School", value: 4 }, { name: "TMD", value: 4 }, { name: "Mart", value: 0 }, { name: "INTER-L", value: 2 },]
    const p100 = '#ff0022'
    const p75 = '#ff7d22'
    const p50 = '#ffff55'
    const p25 = '#aaff55'
    const p0 = '#00ff55'
    const history = useHistory()
    const IFloor = [IFloor1, IFloor2, IFloor3]
    const { id } = useParams();
    const ImageUrl = IFloor[id - 1]
    var ShopMap = {
        name: "my-map",
        areas: [
            { name: "School", shape: "poly", coords: [104, 38, 200, 38, 200, 100, 175, 100, 157, 120, 158, 125, 110, 125, 102, 120, 93, 53], preFillColor: percent[0] },
            { name: "TMD", shape: "poly", coords: [265, 38, 290, 38, 290, 100, 265, 100], preFillColor: percent[2] },
            { name: "Mart", shape: "poly", coords: [290, 38, 312, 38, 312, 100, 290, 100], preFillColor: percent[1] },
            { name: "INTER-L", shape: "poly", coords: [450, 38, 540, 38, 540, 100, 450, 100], preFillColor: percent[4] },
        ]
    }
    const clicked = (area) => {
        console.log(area.name)
        let path = '/shop/' + area.name
        history.push(path)
    }
    const initMap = () => {
        ShopMap.areas.forEach(function (item, key) {
            if (item.name === hValue[key].name) {
                let i = hValue[key].value
                item.preFillColor = percent[i]
                console.log(item.preFillColor);
            }
        });
    }
    useEffect(() => {
        console.log(ShopMap)
        initMap()
        console.log(ShopMap)
    })
    return (
        <Layout>
            <h1>Three Eyes Plug Store</h1>
            <h2>ท่านกำลังอยู่ชั้นที่ {id}</h2>
            <Container>
                <ImageMapper src={ImageUrl} map={ShopMap} onClick={(area, evt) => clicked(area, evt)} />
                <Row>
                    <Col sm='2'><div className='p100'></div>มีจำนวนคนมาก</Col>
                    <Col sm='2'><div className='p75'></div>มีจำนวนคนค่อนข้างเยอะ</Col>
                    <Col sm='2'><div className='p50'></div>มีจำนวนคนปานกลาง</Col>
                    <Col sm='2'><div className='p25'></div>มีจำนวนคนค่อนข้าน้อย</Col>
                    <Col sm='2'><div className='p0'></div>มีจำนวนคน้อย</Col>
                </Row>
            </Container>
        </Layout>
    );
}

export default Floor;
