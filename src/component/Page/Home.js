import React from 'react';
import Logo from '../Image/logo.png'
import {Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarText } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Home.css';

const HomePage = props  => {
    return (
        <div>
            <img src={Logo} className='logo'></img>
            <h1>Three Eyes Plug Store</h1>
            <Link to='/floor/1'>Login</Link>
        </div>
    );
}

export default HomePage;
