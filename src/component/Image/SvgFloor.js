import * as React from "react";

const SvgFloor = props =>{
  return (
    <svg width={600} height={800} viewBox="0 0 158.75 211.667" {...props}>
      <g fill="gray" stroke="#000" strokeWidth={0.707}>
        <path d="M1.89 1.89h52.917v39.688H1.89zM1.89 41.578h52.917v39.688H1.89z" />
        <path d="M1.89 81.265h52.917v39.688H1.89zM1.89 120.953h52.917v39.688H1.89z" />
        <path d="M1.89 160.64h52.917v39.688H1.89zM104.321 1.89h52.917v39.688h-52.917zM104.321 41.578h52.917v39.688h-52.917z" />
        <path d="M104.321 81.265h52.917v39.688h-52.917zM104.321 120.953h52.917v39.688h-52.917z" />
        <path d="M104.321 160.64h52.917v39.688h-52.917z" />
      </g>
    </svg>
  );
}

export default SvgFloor;
